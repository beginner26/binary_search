function binarySearch(array, item) {
    function recurse(min, max) {
      // apabila tidak ada item dalam array, kembalikan -1
      if (min > max) {
        return -1; // Stop Fungsi
      }

      // tentukan middle pointer
      let middle = Math.floor((min + max) / 2); // (0 + 7-1) / 2 = 3
      // jika item yang dicari sama dengan item yang dipointer middle maka kembalikan index middle
      if (array[middle] === item) {
        // item ketemu, kembalikan index middle
        return middle;
      }
      
      // jika item dipointer middle lebih dari item yang dicari
      if (array[middle] > item) {
        // maka panggil fungsi recurse dengan parameter (0, 2)
        return recurse(min, middle - 1);
      }
      
      // jalankan fungsi recurse dengan paramter(2+1, )
      return recurse(middle + 1, max);
    }
    
    // jalankan fungsi recurse(0, 6)
    return recurse(0, array.length - 1);
  }
  
  
  console.log(binarySearch([1, 2, 3, 4, 6, 8, 9], 8)); // 5
  console.log(binarySearch([1, 2, 3, 4, 6, 8, 9], 7)); // -1
  console.log(binarySearch([1, 2, 3, 4, 6, 8, 9], 9)); // 6
  console.log(binarySearch([1], 0)); // -1